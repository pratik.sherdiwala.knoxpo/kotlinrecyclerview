package com.example.kotlindemo1

import android.view.View
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AnimalVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val animalNameTV = itemView.findViewById<TextView>(R.id.animalNameTV)
    private val animalIdTV = itemView.findViewById<TextView>(R.id.animalId)
    private val removeAnimalIb = itemView.findViewById<ImageButton>(R.id.removeIB)
    private val checkAniamlCB = itemView.findViewById<CheckBox>(R.id.animalCb)

    fun bindAnimal(
        animal: Animal,
        deleteAnimal: (animal: Animal) -> Unit,
        checkAnimal: (animal: Animal, isChecked: Boolean) -> Unit
    ) {
        animalNameTV.text = animal.name
        animalIdTV.text = animal.id.toString()

        removeAnimalIb.setOnClickListener {
            deleteAnimal(animal)
        }

        checkAniamlCB.setOnCheckedChangeListener(null)

        checkAniamlCB.isChecked = animal.isChecked

        checkAniamlCB.setOnCheckedChangeListener { buttonView, isChecked ->
            checkAnimal(animal, isChecked)
        }

    }

//    fun bindAnimal(animal: Animal, animalListner: AnimalListner) {
//        animalNameTV.text = animal.name
//        animalIdTV.text = animal.id.toString()
//
//        removeAnimalIb.setOnClickListener {
//            animalListner.onDeleteAnimal(adapterPosition)
//        }
//    }
}