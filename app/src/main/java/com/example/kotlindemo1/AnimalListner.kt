package com.example.kotlindemo1

import java.text.FieldPosition

interface AnimalListner{

    fun onDeleteAnimal(animal: Animal)

    //fun onDeleteAnimal(position:Int)
    fun updateAnimal(animal:Animal, isChecked: Boolean)
}