package com.example.kotlindemo1

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), AnimalListner {


//    private val animals = listOf(
//        "Dog",
//        "Cat",
//        "Lion",
//        "Duck",
//        "Hen",
//        "Elephant",
//        "Fox",
//        "Snake",
//        "Monkey",
//        "Kutro",
//        "Bilado",
//        "Vandro"
//    )

    var animals = ArrayList<Animal>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        animalRV.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        animalRV.adapter = AnimalAdapter(animals, this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_animal, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.addAnimal -> {
                add(animals.size)
            }

            R.id.removeAnimal -> {
                if (animals.size <= 0) {
                    Toast.makeText(this, "No Animal", Toast.LENGTH_SHORT).show()
                } else {
                    remove(animals.size - 1)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun add(position: Int) {
        animals.add(Animal("Animal ", position + 1))
        animalRV.adapter?.notifyDataSetChanged()
    }

    private fun remove(position: Int) {
        animals.removeAt(position)
        animalRV.adapter?.notifyDataSetChanged()
    }

//

    override fun onDeleteAnimal(animal: Animal) {
        animals.remove(animal)
        animalRV.adapter?.notifyDataSetChanged()
    }

    override fun updateAnimal(animal: Animal, isChecked: Boolean) {
        animal.isChecked = isChecked

        if (isChecked) {
            for (element in animals) {
                if (element != animal) {
                    element.isChecked = false
                }
            }
        }
        animalRV.adapter?.notifyDataSetChanged()
    }
}
