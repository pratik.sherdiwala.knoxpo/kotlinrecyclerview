package com.example.kotlindemo1

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


class AnimalAdapter(private val items: ArrayList<Animal>, private val animalListner: AnimalListner) :
    RecyclerView.Adapter<AnimalVH>() {

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: AnimalVH, position: Int) {
        holder.bindAnimal(
            items.get(position),
            {
                animalListner.onDeleteAnimal(it)
            },
            { animal, isChecked ->
                animalListner.updateAnimal(animal, isChecked)
            }
        )
        //holder.bindAnimal(items.get(position),animalListner)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimalVH {
        return AnimalVH(
            LayoutInflater
                .from(parent.context)
                .inflate(
                    R.layout.item_animal,
                    parent,
                    false
                )
        )
    }
}